import 'package:api_meteo/src/model/coord.dart';
import 'package:api_meteo/src/model/main_weather.dart';
import 'package:api_meteo/src/model/weather.dart';

class CurrentWeatherData {
  final Coord coord;
  final List<Weather> weather;
  final String base;
  final MainWeather main;
  

  final int dt;
  final int timezone;
  final int id;
  final String name;
  final int cod;

  CurrentWeatherData(
      {this.coord,
      this.weather,
      this.base,
      this.main,
      this.dt,
      this.timezone,
      this.id,
      this.name,
      this.cod});

  factory CurrentWeatherData.fromJson(dynamic json) {
    if (json == null) {
      return CurrentWeatherData();
    }

    return CurrentWeatherData(
      coord: Coord.fromJson(json['coord']),
      weather: (json['weather'] as List)
              ?.map((w) => Weather.fromJson(w))
              ?.toList() ??
          List.empty(),
      base: json['base'],
      main: MainWeather.fromJson(json['main']),
      dt: json['dt'],
      timezone: json['timezone'],
      id: json['id'],
      name: json['name'],
      cod: json['cod'],
    );
  }
}
